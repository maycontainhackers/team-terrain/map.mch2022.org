#!/bin/bash -e

cd /workdir/map
git fetch origin
if [[ $(git rev-parse master) != $(git rev-parse origin/master) ]]; then
	git reset --hard
	git checkout master
	git pull --ff-only origin master
	
	/build.sh
fi
