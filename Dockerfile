FROM debian:bullseye

RUN \
	   apt-get update \
	&& apt-get install -y --no-install-recommends \
		aptitude \
		less \
		nano \
		build-essential \
		libpq-dev \
		git \
		wget \
		gdal-bin \
		python3-pip \
		libpython3-dev \
		postgresql-postgis \
		cron \
	#&& rm -rf /var/lib/apt/lists/*
	&& true

RUN \
	   service postgresql start \
	&& su postgres -c "psql -c \"CREATE USER root;\"" \
	&& su postgres -c "createdb -O root buildmap" \
	&& su postgres -c "psql -d buildmap -c 'CREATE EXTENSION postgis;'" \
	&& service postgresql stop

RUN \
	   cd / \
	&& git clone https://gitlab.com/maycontainhackers/team-terrain/buildmap.git -b mch2022 \
	&& cd /buildmap \
	&& pip install -e /buildmap

COPY start.sh update.sh build.sh local.conf.json /
COPY crontab /etc

WORKDIR /
CMD exec /start.sh
