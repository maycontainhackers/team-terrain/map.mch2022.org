#!/bin/bash -e

TARGET=/output/www

VERSIONDIR="${TARGET}"-$(date +%s)
if [[ -e ${VERSIONDIR} ]]; then
	VERSIONDIR=$(mktemp -d ${VERSIONDIR}-XXXXXXXXX)
else
	mkdir -p ${VERSIONDIR}
fi

cp -R /workdir/map/www/* "${VERSIONDIR}"

BUILDMAPDIR=$(mktemp -d)
cd "${BUILDMAPDIR}"
mkdir buildmap-output
cp /local.conf.json .
rm -rf /tmp/buildmap
buildmap /workdir/map/map.conf.json local.conf.json

cp -R buildmap-output/* "${VERSIONDIR}"

cd /
rm -rf "${BUILDMAPDIR}"

ln -s $(basename ${VERSIONDIR}) ${TARGET}
