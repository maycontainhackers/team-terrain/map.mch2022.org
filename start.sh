#!/bin/bash -e

/etc/init.d/postgresql start

cd /
mkdir -p /workdir
cd /workdir
if [[ -e map ]]; then
	cd map
	git reset --hard
	git checkout master
	git pull --ff-only origin master
else
	git clone https://gitlab.com/maycontainhackers/team-terrain/map.git
fi

/build.sh

cron -f

/etc/init.d/postgresql stop
